from flask import Flask
from movies.models import db
from movies.views import api

app: Flask = Flask(__name__)
# Init db
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

db.init_app(app)
app.register_blueprint(api)


@app.before_first_request
def create_db() -> None:
    db.create_all()
