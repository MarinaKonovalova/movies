import base64
import typing as t
from functools import wraps
from http import HTTPStatus

import pydantic as pd
from flask import Blueprint, request
from movies import schemas

from .models import Comment, Film, Rating, User, db

api: Blueprint = Blueprint("api", __name__)


@api.errorhandler(pd.ValidationError)
def handle_bad_request(error: pd.ValidationError) -> t.Tuple[str, HTTPStatus]:
    return error.json(), HTTPStatus.BAD_REQUEST


def is_authorized(
    func: t.Callable[[], t.Tuple[str, HTTPStatus]]
) -> t.Callable[[], t.Tuple[str, HTTPStatus]]:
    @wraps(func)
    def wrapper():  # type: ignore
        auth_type, _, token = request.headers.get("Authorization", "").partition(" ")
        if auth_type == "Basic":
            login, _, password = base64.b64decode(token).decode().partition(":")
            user: User = User.query.filter_by(login=login).one_or_none()
            if user and user.password == password:
                return func()
        return schemas.Error(error="Unauthorized").json(), HTTPStatus.UNAUTHORIZED

    return wrapper


def get_user() -> User:
    _, _, token = request.headers.get("Authorization", "").partition(" ")
    login, _, _ = base64.b64decode(token).decode().partition(":")
    return User.query.filter_by(login=login).one_or_none()


@api.route("/users/", methods=["POST"])
def create_user() -> t.Tuple[str, HTTPStatus]:
    data: schemas.CreateUserRequest = schemas.CreateUserRequest.parse_raw(request.data)
    user: User = User.query.filter_by(login=data.login).one_or_none()
    if user:
        return schemas.Error(error="User already exists").json(), HTTPStatus.CONFLICT

    user = User(login=data.login, password=data.password)
    db.session.add(user)
    db.session.commit()
    return schemas.UserResponse.from_orm(user).json(), HTTPStatus.CREATED


@api.route("/films/", methods=["POST"])
@is_authorized
def create_film() -> t.Tuple[str, HTTPStatus]:
    data: schemas.CreateFilm = schemas.CreateFilm.parse_raw(request.data)
    film: Film = Film.query.filter_by(name=data.name).one_or_none()
    if film:
        return schemas.Error(error="Film already exists").json(), HTTPStatus.CONFLICT

    film = Film(name=data.name, year_of_release=data.year_of_release)
    db.session.add(film)
    db.session.commit()
    return schemas.Film.from_orm(film).json(), HTTPStatus.CREATED


@api.route("/films/", methods=["GET"])
@is_authorized
def film_list() -> t.Tuple[str, HTTPStatus]:
    data: schemas.FilmListFilter = schemas.FilmListFilter.parse_obj(request.args)
    avg_rating_col = db.func.avg(Rating.rating).label("avg_rating")
    films = (
        db.session.query(
            Film, avg_rating_col, db.func.count(Rating.rating).label("rating_count")
        )
        .outerjoin(Rating, Film.id == Rating.film_id)
        .group_by(Film.id)
    )

    if data.name:
        films = films.filter(Film.name.ilike(data.name))
    if data.year_of_release:
        films = films.filter(Film.year_of_release == data.year_of_release)
    if data.sort_by_rating:
        films = films.order_by(avg_rating_col.desc())

    films_pag = films.offset(data.offset).limit(data.limit).subquery()

    films_res = (
        db.session.query(films_pag, db.func.count(Comment.id).label("comment_count"))
        .outerjoin(Comment, films_pag.c.film_id == Comment.film_id)
        .group_by(films_pag.c.film_id)
        .all()
    )

    return (
        str([schemas.FilmListResponse.from_orm(f).json() for f in films_res]),
        HTTPStatus.OK,
    )


@api.route("/ratings/", methods=["POST"])
@is_authorized
def add_rating() -> t.Tuple[str, HTTPStatus]:
    user: User = get_user()
    data: schemas.AddRating = schemas.AddRating.parse_raw(request.data)
    film: Film = Film.query.get(data.film_id)
    if not film:
        return schemas.Error(error="Film not found").json(), HTTPStatus.NOT_FOUND

    rating: Rating = Rating(user_id=user.id, rating=data.rating, film_id=film.id)
    db.session.add(rating)
    db.session.commit()

    return schemas.Rating.from_orm(rating).json(), HTTPStatus.CREATED


@api.route("/comments/", methods=["POST"])
@is_authorized
def add_comment() -> t.Tuple[str, HTTPStatus]:
    user: User = get_user()
    data: schemas.AddComment = schemas.AddComment.parse_raw(request.data)
    film: Film = Film.query.get(data.film_id)
    if not film:
        return schemas.Error(error="Film not found").json(), HTTPStatus.NOT_FOUND

    comment: Comment = Comment(user_id=user.id, text=data.text, film_id=film.id)
    db.session.add(comment)
    db.session.commit()

    return schemas.Comment.from_orm(comment).json(), HTTPStatus.CREATED


@api.route("/comments/", methods=["GET"])
@is_authorized
def get_comments() -> t.Tuple[str, HTTPStatus]:
    data: schemas.FilmCommentsList = schemas.FilmCommentsList.parse_raw(request.data)
    comments = (
        db.session.query(Comment)
        .outerjoin(Film)
        .filter(Film.id == data.film_id)
        .offset(data.offset)
        .limit(data.limit)
        .all()
    )
    return (
        str([schemas.Comment.from_orm(comment).json() for comment in comments]),
        HTTPStatus.OK,
    )
