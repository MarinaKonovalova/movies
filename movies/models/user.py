import sqlalchemy_utils as su

from .base import db


class User(db.Model):
    __tablename__: str = "users"

    id: db.Column = db.Column(
        "user_id", db.Integer(), primary_key=True, autoincrement=True
    )
    login: db.Column = db.Column(db.String(50), nullable=False, unique=True)
    password: db.Column = db.Column(su.PasswordType(schemes=["pbkdf2_sha512"]))
    comments: db.relationship = db.relationship("Comment", backref="user")
    ratings: db.relationship = db.relationship("Rating", backref="user")
