from .base import db


class Comment(db.Model):
    __tablename__: str = "comments"

    id: db.Column = db.Column(
        "comment_id", db.Integer(), primary_key=True, autoincrement=True
    )
    text: db.Column = db.Column(db.String(500), nullable=False)
    user_id: db.Column = db.Column(db.Integer(), db.ForeignKey("users.user_id"))
    film_id: db.Column = db.Column(db.Integer(), db.ForeignKey("films.film_id"))
