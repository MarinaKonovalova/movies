from .base import db


class Film(db.Model):
    __tablename__: str = "films"

    id: db.Column = db.Column(
        "film_id", db.Integer(), primary_key=True, autoincrement=True
    )
    name: db.Column = db.Column(db.String(100), nullable=False, unique=True)
    year_of_release: db.Column = db.Column(db.Integer(), nullable=False)
    comments: db.Column = db.relationship("Comment", backref="films")
    ratings: db.Column = db.relationship("Rating", backref="films")
