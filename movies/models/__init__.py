# flake8: noqa

from .comment import Comment
from .film import Film
from .rating import Rating
from .user import User
from .base import db
