from .base import db


class Rating(db.Model):
    __tablename__: str = "ratings"

    id: db.Column = db.Column(
        "rating_id", db.Integer(), primary_key=True, autoincrement=True
    )
    rating: db.Column = db.Column(db.Integer(), nullable=False)
    user_id: db.Column = db.Column(db.Integer(), db.ForeignKey("users.user_id"))
    film_id: db.Column = db.Column(db.Integer(), db.ForeignKey("films.film_id"))
