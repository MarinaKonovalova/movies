import typing as t

import pydantic as pd


class BaseModel(pd.BaseModel):
    class Config:
        orm_mode = True


class BaseResponse(BaseModel):
    data: t.Any


class CreateUserRequest(BaseModel):
    login: str
    password: str


class UserResponse(BaseModel):
    id: int
    login: str


class CreateFilm(BaseModel):
    name: str
    year_of_release: int


class Film(BaseModel):
    id: int
    name: str
    year_of_release: int
    avg_rating: t.Optional[float]
    rating_count: t.Optional[int] = 0
    comment_count: t.Optional[int] = 0


class Comment(BaseModel):
    id: int
    user_id: int
    text: str


class Rating(BaseModel):
    id: int
    user_id: int
    rating: int


class FilmListFilter(BaseModel):
    name: t.Optional[str]
    year_of_release: t.Optional[int]
    sort_by_rating: t.Optional[bool]
    offset: t.Optional[int] = 0
    limit: t.Optional[int] = 10


class FilmListResponse(BaseResponse):
    avg_rating: t.Optional[int]
    comment_count: int
    film_id: int
    name: str
    rating_count: int
    year_of_release: int


class AddRating(BaseModel):
    film_id: int
    rating: int = pd.Field(..., ge=0, le=10)


class AddComment(BaseModel):
    film_id: int
    text: str


class FilmCommentsList(BaseModel):
    film_id: int
    offset: t.Optional[int] = 0
    limit: t.Optional[int] = 10


class FilmCommentsListResponse(BaseModel):
    data: t.List[Comment]


class Error(BaseModel):
    error: str
