import base64
import json
import typing as t
from http import HTTPStatus

import flask
import pytest
from flask import Flask
from flask.ctx import AppContext
from flask.testing import FlaskClient
from movies import schemas
from movies.models import Film, User, db
from movies.views import api


@pytest.fixture(scope="module")
def test_client() -> t.Generator[FlaskClient, None, None]:
    flask_app: Flask = Flask(__name__)

    flask_app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    flask_app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    db.init_app(flask_app)
    flask_app.register_blueprint(api)
    testing_client: FlaskClient = flask_app.test_client()
    ctx: AppContext = flask_app.app_context()
    ctx.push()
    yield testing_client
    ctx.pop()


@pytest.fixture(scope="module")
def user() -> t.Dict[str, str]:
    return {"login": "user1", "password": "kek"}


@pytest.fixture(scope="module")
def init_database(user: t.Dict[str, str]) -> t.Generator[None, None, None]:
    db.create_all()
    film1: Film = Film(name="film_name", year_of_release=2000)
    user1: User = User(login=user["login"], password=user["password"])
    db.session.add(user1)
    db.session.add(film1)
    db.session.commit()
    yield
    db.drop_all()


@pytest.fixture(scope="module")
def header_auth(user: t.Dict[str, str]) -> t.Dict[str, str]:
    credentials = base64.b64encode(
        bytes(":".join([user["login"], user["password"]]), "utf-8")
    ).decode()
    auth = "Basic {}".format(credentials)
    return {"Authorization": auth}


@pytest.mark.parametrize("url", ["/films/", "/comments/"])
def test_authorization(test_client: FlaskClient, url: str, init_database: None) -> None:
    response: flask.wrappers.Response = test_client.get(url)
    assert schemas.Error(error="Unauthorized").json() == response.data.decode()
    assert response.status_code == HTTPStatus.UNAUTHORIZED


def test_validation_error(test_client: FlaskClient, init_database: None) -> None:
    response: flask.wrappers.Response = test_client.post("/users/", data=json.dumps({}))
    assert response.status_code == HTTPStatus.BAD_REQUEST


def test_post_user(test_client: FlaskClient, init_database: None) -> None:
    response: flask.wrappers.Response = test_client.post(
        "/users/", data=json.dumps({"login": "user2", "password": "lol"})
    )
    assert schemas.UserResponse(id=2, login="user2").json() == response.data.decode()
    assert response.status_code == HTTPStatus.CREATED


@pytest.mark.parametrize(
    "url",
    [
        "/films/",
        "/films/?sort_by_rating=1",
        "/films/?name=film_name",
        "/films/?year_of_release=2000",
    ],
)
def test_get_films(
    test_client: FlaskClient,
    init_database: None,
    header_auth: t.Dict[str, str],
    url: str,
) -> None:
    response: flask.wrappers.Response = test_client.get(url, headers=header_auth)
    assert (
        str(
            [
                (
                    schemas.FilmListResponse(
                        comment_count=0,
                        film_id=1,
                        name="film_name",
                        rating_count=0,
                        year_of_release=2000,
                    ).json()
                )
            ]
        )
        == response.data.decode()
    )

    assert response.status_code == HTTPStatus.OK


def test_get_films_year_of_release_2(
    test_client: FlaskClient, init_database: None, header_auth: t.Dict[str, str]
) -> None:
    response: flask.wrappers.Response = test_client.get(
        "/films/?year_of_release=2010", headers=header_auth
    )
    assert response.data.decode() == "[]"
    assert response.status_code == HTTPStatus.OK


def test_post_film(
    test_client: FlaskClient, init_database: None, header_auth: t.Dict[str, str]
) -> None:
    response: flask.wrappers.Response = test_client.post(
        "/films/",
        data=json.dumps({"name": "film2", "year_of_release": 2020}),
        headers=header_auth,
    )

    assert (
        schemas.Film(
            id=2,
            name="film2",
            year_of_release=2020,
            avg_rating=None,
            rating_count=0,
            comment_count=0,
        ).json()
        == response.data.decode()
    )
    assert response.status_code == HTTPStatus.CREATED


def test_post_rating(
    test_client: FlaskClient, init_database: None, header_auth: t.Dict[str, str]
) -> None:
    response: flask.wrappers.Response = test_client.post(
        "/ratings/", data=json.dumps({"film_id": 1, "rating": 5}), headers=header_auth,
    )
    assert schemas.Rating(id=1, user_id=1, rating=5).json() == response.data.decode()
    assert response.status_code == HTTPStatus.CREATED


def test_post_comment(
    test_client: FlaskClient, init_database: None, header_auth: t.Dict[str, str]
) -> None:
    response: flask.wrappers.Response = test_client.post(
        "/comments/",
        data=json.dumps({"film_id": 1, "text": "text_comment"}),
        headers=header_auth,
    )
    assert (
        schemas.Comment(id=1, user_id=1, text="text_comment").json()
        == response.data.decode()
    )
    assert response.status_code == HTTPStatus.CREATED


def test_get_comments(
    test_client: FlaskClient, init_database: None, header_auth: t.Dict[str, str]
) -> None:
    response: flask.wrappers.Response = test_client.get(
        "/comments/", data=json.dumps({"film_id": 2}), headers=header_auth,
    )
    assert response.data.decode() == "[]"
    assert response.status_code == HTTPStatus.OK
